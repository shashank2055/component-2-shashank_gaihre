﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphical_Programming_application
{
    /// <summary>
    /// triangle shape class defined
    /// </summary>
    class Triangle : Shape
    {
        /// <summary>
        /// variable declertions
        /// </summary>
        PointF p1, p2, p3;

        /// <summary>
        ///  cordinates of triangle
        /// </summary>
        /// <param name="point1"></param>
        /// <param name="point2"></param>
        /// <param name="point3"></param>
        /// <param name="point4"></param>
        /// <param name="point5"></param>
        /// <param name="point6"></param>
        public void setPoint(int point1, int point2, int point3, int point4, int point5, int point6)
        {
            this.p1 = new PointF(point1, point2);
            this.p2 = new PointF(point3, point4);
            this.p3 = new PointF(point5, point6);
        }

        /// <summary>
        /// setting graphics, color and thickness for triangle
        /// </summary>
        /// <param name="g"></param>
        /// <param name="c"></param>
        /// <param name="thickness"></param>
        public override void draw(Graphics g, Color c, int thickness)
        {

            Pen p = new Pen(c, thickness);
            PointF[] curvePoints =
            {
                p1,p2,p3
            };
            g.DrawPolygon(p, curvePoints);
            SolidBrush b = new SolidBrush(colour);
            g.FillPolygon(b, curvePoints);

        }

        public override double calcArea()
        {
            throw new NotImplementedException();
        }

        public override double calcPerimeter()
        {
            throw new NotImplementedException();
        }

        
    }
}