﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Graphical_Programming_application
{
    /// <summary>
    /// circle shape class defined
    /// </summary>
    class Circle : Shape
    {
        /// <summary>
        /// variable declertions
        /// </summary>
        int radius;

        /// <summary>
        /// constructor
        /// </summary>
        public Circle() : base()
        {

        }
        public Circle(Color colour, int x, int y, int radius) : base(colour, x, y)
        {

            this.radius = radius; //the only thingthat is different from shape
        }


        public void setRadius(int radius) { 
            this.radius = radius;


        }


        /// <summary>
        /// graphics, color and thickness 
        /// </summary>
        /// <param name="g"></param>
        /// <param name="c"></param>
        /// <param name="thickness"></param>
        public override void draw(Graphics g,Color c, int thickness)
        {

            Pen p = new Pen(c, thickness);
            g.DrawEllipse(p, x, y, radius * 2, radius * 2);

        }
        /// <summary>
        /// area of circle 
        /// </summary>
        /// <returns></returns>

        public override double calcArea()
        {
            return Math.PI * (radius ^ 2);
        }

        public override double calcPerimeter()
        {
            return 2 * Math.PI * radius;
        }

        public override string ToString() //all classes inherit from object and ToString() is abstract in object
        {
            return base.ToString() + "  " + this.radius;
        }
    }
}
