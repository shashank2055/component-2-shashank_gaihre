﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Graphical_Programming_application
{
    public partial class Form1 : Form
    {

        
        Bitmap outputBitmap = new Bitmap(640, 500);
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            outputBitmap = new Bitmap(640, 500);
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            g.DrawImageUnscaled(outputBitmap, 0, 0);

        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Text Files|*.txt";

            if (sfd.ShowDialog() == DialogResult.OK)
            {
                System.IO.File.WriteAllText(sfd.FileName, richTextBox1.Text);
            }
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Text Files|*.txt";

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                richTextBox1.Text = System.IO.File.ReadAllText(ofd.FileName);
            }
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {

            MessageBox.Show("For Your Help:\n" +
                "draw circle 60\n" +
                "draw rectangle 100 50\n" +
                "draw triangle 10 100 100 10 200 200\n" +
                "draw square 50\n" +
                "move 100 100\n" +
                "color red 5\n" +
                "fill red\n" +
                "fill no\n");
        }

        private void button1_Click(object sender, EventArgs e)
        {


            string cmm = textBox1.Text.ToLower();
            string cod = richTextBox1.Text.ToLower();


            CodeParse cm = new CodeParse();
            string parsedCode = cm.Parse(cmm, cod);
            Graphics g = Graphics.FromImage(outputBitmap);
            g.DrawImageUnscaled(cm.GetBitmap(), 0, 0);
            panel1.Refresh();
            if (parsedCode == "clear")
            {
                this.outputBitmap.Dispose();
                textBox1.Clear();
                Refresh();
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void richTextBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {

                string cod = textBox1.Text.ToLower();


                CodeParse cm = new CodeParse();
                string parsedCode = cm.Parse("run", cod);
                if (parsedCode != null)
                {
                    MessageBox.Show(parsedCode);
                }
            }
        }
    }
}
