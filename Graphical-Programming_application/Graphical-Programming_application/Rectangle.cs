﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Graphical_Programming_application
{
    /// <summary>
    /// rectangle shape class defined
    /// </summary>
    class Rectangle : Shape
    {
        /// <summary>
        /// variable declertions
        /// </summary>
        int width, height;
        /// <summary>
        /// constructor
        /// </summary>
        public Rectangle() : base()
        {
            width = 100;
            height = 100;
        }
        public Rectangle(Color colour, int x, int y, int width, int height) : base(colour, x, y)
        {

            this.width = width; //the only thingthat is different from shape
            this.height = height;
        }

        public void setHeight(int height)
        {
            this.height = height;


        }
        /// <summary>
        /// declaring  width of the rectangle
        /// </summary>
        /// <param name="width"></param>
        public void setWidth(int width)
        {
            this.width = width;


        }
        /// <summary>
        /// graphics, color and thickness
        /// </summary>
        /// <param name="g"></param>
        /// <param name="c"></param>
        /// <param name="thickenss"></param>
        public override void draw(Graphics g,Color c, int thickenss)
        {
            Pen p = new Pen(c, thickenss);
            g.DrawRectangle(p, x, y, width, height);
        }

        public override double calcArea()
        {
            return width * height;
        }

        public override double calcPerimeter()
        {
            return 2 * width + 2 * height;
        }
    }
}
