﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphical_Programming_application
{
    /// <summary>
    /// defining shapefactorydefination class
    /// </summary>
    public class ShapeFactoryDefination
    {
        /// <summary>
        /// summarizing all the shape to be executed 
        /// </summary>
        /// <param name="shape"></param>
        /// <returns></returns>
        public bool isCircle(string shape)
        {
            if (shape == "circle")
            {
                return true;
            }
            return false;
        }
        public bool isTriangle(string shape)
        {
            if (shape == "triangle")
            {
                return true;
            }
            return false;
        }
        public bool isRectangle(string shape)
        {
            if (shape == "rectangle")
            {
                return true;
            }
            return false;
        }
        public bool isSquare(string shape)
        {
            if (shape == "square")
            {
                return true;
            }
            return false;
        }
    }
}
