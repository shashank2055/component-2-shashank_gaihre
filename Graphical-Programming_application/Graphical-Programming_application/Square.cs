﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Graphical_Programming_application
{
    /// <summary>
    /// square shape class defined
    /// </summary>
    class Square : Rectangle
    {
        /// <summary>
        /// variable declaration
        /// </summary>
        private int size;

        public Square() : base()
        {

        }
        /// <summary>
        /// colour and sze setup
        /// </summary>
        /// <param name="colour"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="size"></param>
        public Square(Color colour, int x, int y, int size) : base(colour, x, y, size, size)
        {
            this.size = size;
        }

        public void setSize(int size)
        {
            this.size = size;
        }
        
    }
}
